<?php
/*
Plugin Name: Preiser Single Post
Plugin URI:  https://cadence-labs.com
Description: Divi Extension for a Single Post
Version:     1.0.0
Author:      Cadence Labs
Author URI:  https://cadence-labs.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: prsp-preiser-single-post
Domain Path: /languages

Preiser Single Post is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Preiser Single Post is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Preiser Single Post. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/


if ( ! function_exists( 'prsp_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function prsp_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/PreiserSinglePost.php';
}
add_action( 'divi_extensions_init', 'prsp_initialize_extension' );
endif;
