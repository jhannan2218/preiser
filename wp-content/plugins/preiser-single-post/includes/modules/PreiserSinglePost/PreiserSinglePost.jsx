// External Dependencies
import React, { Component, Fragment } from 'react';

// Internal Dependencies
import './style.css';


class PreiserSinglePost extends Component {

  static slug = 'prsp_preiser_single_post_module';

  render() {
    return (
        <Fragment>
            <span className="post-category">{this.props.category}</span>
            <div className="post-image"><img src={this.props.image} alt={this.props.image.alt} /></div>
            <div className="post-content">
                <h1 className="preiser-single-post-heading">{this.props.heading}</h1>
                <p>{this.props.content()}</p>
                <a href={this.props.button} className="btn et_pb_button">Call to Action</a>
            </div>
        </Fragment>
    );
  }
}

export default PreiserSinglePost;
