<?php

class PRSP_PreiserSinglePostModule extends ET_Builder_Module {

	public $slug       = 'prsp_preiser_single_post_module';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => 'https://cadence-labs.com',
		'author'     => 'Cadence Labs',
		'author_uri' => 'https://cadence-labs.com',
	);

	public function init() {
		$this->name = esc_html__( 'Single Post', 'prsp-preiser-single-post' );
	}

	public function get_fields() {
		return array(
            'image'     => array(
                'label'           => esc_html__( 'Image', 'prsp-preiser-single-post' ),
                'type'            => 'upload',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Main Image for Post.', 'prsp-preiser-single-post' ),
                'toggle_slug'     => 'main_content',
            ),
            'category'     => array(
                'label'           => esc_html__( 'Category', 'prsp-preiser-single-post' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Post Category.', 'prsp-preiser-single-post' ),
                'toggle_slug'     => 'main_content',
            ),
            'heading'     => array(
                'label'           => esc_html__( 'Heading', 'prsp-preiser-single-post' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Input your desired heading here.', 'prsp-preiser-single-post' ),
                'toggle_slug'     => 'main_content',
            ),
            'content'     => array(
                'label'           => esc_html__( 'Content', 'prsp-preiser-single-post' ),
                'type'            => 'tiny_mce',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Content entered here will appear below the heading text.', 'prsp-preiser-single-post' ),
                'toggle_slug'     => 'main_content',
            ),
            'button'     => array(
                'label'           => esc_html__( 'Button URL', 'prsp-preiser-single-post' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__( 'Single Post Destination.', 'prsp-preiser-single-post' ),
                'toggle_slug'     => 'main_content',
            ),
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
        return sprintf(
            '<span class="post-category post-cat-%1$s">%2$s</span>
            <div class="post-image"><img src="%3$s" alt="%4$s" /></div>
            <div class="post-content">
            <h3 class="single-post-header">%4$s</h3>
            %5$s
            <a href="%6$s" class="btn et_pb_button">Call To Action</a>
            </div>',
            esc_html( strtolower( $this->props['category']) ),
            esc_html( $this->props['category'] ),
            $this->props['image'],
            esc_html( $this->props['heading'] ),
            $this->props['content'],
            esc_html($this->props['button'])
        );
	}
}

new PRSP_PreiserSinglePostModule;
