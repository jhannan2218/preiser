jQuery(document).ready(function($) {

    var theme = {
        defaults:
        {
            header:
            {
                container: $('#main-header'),
                search: {
                    targetUrl: $('.header-search-form').attr('data-target'),
                    container: $('.header-search-form'),
                    field: $('#input-search')
                }
            },
            modal:
            {
                isActive: false,
                isOpen: false,
                container: $('.popup-modal-container'),
                content: $('.popup-modal-container .popup-modal-content'),
                targetLabel: $('.popup-modal-container .popup-modal-content > p span'),
                button: $('.popup-modal-container .popup-modal-content .et_pb_button.continue'),
                cancelTrigger: $('.popup-modal-container .cancel')
            }
        },
        init: function()
        {
            theme.headerSearch();
            theme.intlDropdown();
            theme.modalListener();
        },
        headerSearch: function()
        {
            theme.defaults.header.search.container.on('submit', function(){
                var value = theme.defaults.header.search.field.val();
                value = value.replace(' ', '%20');
                var searchUrl = theme.defaults.header.search.targetUrl+value;
                window.open(searchUrl, '_blank');
                return false;
            });
        },
        modalListener: function()
        {
            $('#media_image-2 > a').on('click', theme.defaults.modal.container, function(){
                var targetUrl = $(this).attr('href');
                if(!theme.defaults.modal.isActive) {
                    console.log('not active');
                    theme.populateModal(targetUrl);
                    theme.modalOpen();
                    return false;
                }
            });
            theme.defaults.modal.cancelTrigger.on('click', function() {
                theme.modalClose();
                theme.defaults.modal.isActive = false;
            });
        },
        populateModal: function(url)
        {
            theme.defaults.modal.content.html('');
            theme.defaults.modal.content.prepend('<img src="'+url+'" />');
        },
        closeListeners: function()
        {
            theme.defaults.modal.cancelTrigger.on('click', function(){
                theme.modalClose();
                return false;
            });
        },
        modalOpen: function()
        {
            if(!theme.defaults.modal.isOpen) {
                theme.defaults.modal.isOpen = true;
            }
            $(theme.defaults.modal.container).addClass('open');
        },
        modalClose: function()
        {
            if(theme.defaults.modal.isOpen) {
                theme.defaults.modal.isOpen = false;
            }
            theme.defaults.modal.container.removeClass('open');
        },
        intlDropdown: function()
        {
            $('.intl-dropdown-wrapper select').on('change', function(){
                var target = this.value;
                if(target != 'default') {
                    window.open(target);
                }

            });
        }
    }
    theme.init();

});