module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'css/global.css': 'css/source/global.scss'
                }
            }
        },
        postcss: {
            // Begin Post CSS Plugin
            dist: {
                src: 'css/*.css',
            },
        },
        cssmin: {
            // Begin CSS Minify Plugin
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'css',
                        src: ['*.css', '!*.min.css'],
                        dest: 'css',
                        ext: '.min.css'
                    },
                ],
            },
        },
        concat: {
            dist: {
                src: [ 'js/vendor/*.js', 'js/theme/*.js'],
                dest: 'js/theme.js'
            },
        },
        uglify: {
            scripts: {
                options: {
                    sourceMap: true,
                    sourceMapName: 'js/theme.min.js.map'
                },
                files: {
                    'js/theme.min.js': ['js/theme.js']
                },
            },
        },
        watch: {
            css: {
                files: 'css/source/**/*.scss',
                tasks: ['sass', 'postcss', 'cssmin']
            },
            scripts: {
                files: ['js/vendor/*.js', 'js/theme/*.js' ],
                tasks: ['concat', 'uglify']
            }
        }
    });

    // Load Grunt plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);
}