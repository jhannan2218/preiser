
<form target="_blank" method="post" class="header-search-form" data-target="https://portal.distone.com/preiser/#/search/">
    <?php
    printf( '<input type="search" class="et-search-field" id="input-search" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
        esc_attr__( 'Search &hellip;', 'Divi' ),
        get_search_query(),
        esc_attr__( 'Search for:', 'Divi' )
    );

    /**
     * Fires inside the search form element, just before its closing tag.
     *
     * @since ??
     */
    do_action( 'et_search_form_fields' );
    ?>
    <div class="submit"><input type="submit" value="" /></div>
</form>