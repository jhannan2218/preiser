<?php $menuClass = 'nav'; ?>
<nav id="top-menu-nav">
  <div class="container">
<?php
if ( 'on' === et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
$primaryNav = '';

$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );
$accountNav = wp_nav_menu( array( 'theme_location' => 'account-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'account-menu', 'echo' => false ) );
if ( empty( $primaryNav ) ) :
    ?>
    <ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
        <?php if ( 'on' === et_get_option( 'divi_home_link' ) ) { ?>
            <li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
        <?php }; ?>

        <?php show_page_menu( $menuClass, false, false ); ?>
        <?php show_categories_menu( $menuClass, false ); ?>
    </ul>
<?php
else :
    echo et_core_esc_wp( $primaryNav );
    if(!empty($accountNav)) :
      echo et_core_esc_wp( $accountNav );
    endif;
endif;
?>
  </div><!--/container-->
</nav><!--/top-menu-nav-->