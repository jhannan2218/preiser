<div class="top-bar top-bar-header">
    <div class="container">
        <?php
        do_action('header_top_bar_area');
        $args = array(
            'theme_location' => 'top-bar-menu',
            'container' => false
        );
        wp_nav_menu($args);
        ?>
    </div>
</div>