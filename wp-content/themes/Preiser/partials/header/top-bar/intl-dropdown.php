<div class="intl-dropdown-wrapper">
    <select>
        <option value="default">International Sites</option>
        <option value="https://preiser.cn/ ">Chinese</option>
        <option value="https://preiserinternational.com/id/">Indonesian</option>
        <option value="https://preiserinternational.com/ru">Russian</option>
        <option value="https://preiserinternational.com/">Spanish</option>
        <option value="https://preiserinternational.com/pt/">Portuguese</option>
        <option value="https://preiserinternational.com/fr/">French</option>
    </select>
</div>