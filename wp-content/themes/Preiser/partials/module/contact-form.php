<script src="https://cdnjs.cloudflare.com/ajax/libs/punycode/1.4.1/punycode.min.js"></script>
<script src="https://cdn.jotfor.ms/js/vendor/imageinfo.js?v=3.3.21359" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/file-uploader/fileuploader.js?v=3.3.21359"></script>
<script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.21359" type="text/javascript"></script>
<script type="text/javascript">
    JotForm.init(function(){
        if (window.JotForm && JotForm.accessible) $('input_3').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_11').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_12').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_8').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_13').setAttribute('tabindex',0);
        if (window.JotForm && JotForm.accessible) $('input_10').setAttribute('tabindex',0);
        JotForm.newDefaultTheme = false;
        JotForm.extendsNewTheme = false;
        JotForm.newPaymentUIForNewCreatedForms = true;
        JotForm.newPaymentUI = true;
        JotForm.alterTexts(undefined);
        JotForm.clearFieldOnHide="disable";
        setTimeout(function() {
            JotForm.initMultipleUploads();
        }, 2);
        JotForm.submitError="jumpToFirstError";
        /*INIT-END*/
    });

    JotForm.prepareCalculationsOnTheFly([null,{"name":"heading","qid":"1","text":"Form","type":"control_head"},{"name":"submit2","qid":"2","text":"Submit","type":"control_button"},{"description":"","name":"name","qid":"3","subLabel":"","text":"Name","type":"control_textbox"},{"description":"","name":"email","qid":"4","subLabel":"example@example.com","text":"Email","type":"control_email"},null,null,null,{"description":"","name":"companyName","qid":"8","subLabel":"","text":"Company Name","type":"control_textbox"},null,{"description":"","name":"questions","qid":"10","subLabel":"","text":"Questions \u002F Comments","type":"control_textarea"},{"description":"","name":"phoneNumber11","qid":"11","subLabel":"","text":"Phone Number","type":"control_textbox"},{"description":"","name":"country","qid":"12","subLabel":"","text":"Country","type":"control_textbox"},{"description":"","name":"state","qid":"13","subLabel":"","text":"State \u002F Province","type":"control_textbox"},{"description":"","name":"input14","qid":"14","subLabel":"","text":"","type":"control_fileupload"}]);
    setTimeout(function() {
        JotForm.paymentExtrasOnTheFly([null,{"name":"heading","qid":"1","text":"Form","type":"control_head"},{"name":"submit2","qid":"2","text":"Submit","type":"control_button"},{"description":"","name":"name","qid":"3","subLabel":"","text":"Name","type":"control_textbox"},{"description":"","name":"email","qid":"4","subLabel":"example@example.com","text":"Email","type":"control_email"},null,null,null,{"description":"","name":"companyName","qid":"8","subLabel":"","text":"Company Name","type":"control_textbox"},null,{"description":"","name":"questions","qid":"10","subLabel":"","text":"Questions \u002F Comments","type":"control_textarea"},{"description":"","name":"phoneNumber11","qid":"11","subLabel":"","text":"Phone Number","type":"control_textbox"},{"description":"","name":"country","qid":"12","subLabel":"","text":"Country","type":"control_textbox"},{"description":"","name":"state","qid":"13","subLabel":"","text":"State \u002F Province","type":"control_textbox"},{"description":"","name":"input14","qid":"14","subLabel":"","text":"","type":"control_fileupload"}]);}, 20);
</script>
</head>
<body>
<form class="jotform-form" action="https://submit.jotform.com/submit/202537074060043/" method="post" enctype="multipart/form-data" name="form_202537074060043" id="202537074060043" accept-charset="utf-8" autocomplete="on">
  <input type="hidden" name="formID" value="202537074060043" />
  <input type="hidden" id="JWTContainer" value="" />
  <input type="hidden" id="cardinalOrderNumber" value="" />
  <div role="main" class="form-all">
    <div class="form-section page-section">
      <div class="column">
        <div class="form-line" data-type="control_textbox" id="id_3">
          <label class="form-label form-label-top form-label-auto" id="label_3" for="input_3"> Name </label>
          <div id="cid_3" class="form-input-wide">
            <input type="text" id="input_3" name="q3_name" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_3" />
          </div>
        </div>
        <div class="form-line" data-type="control_email" id="id_4">
          <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4"> Email </label>
          <div id="cid_4" class="form-input-wide">
          <span class="form-sub-label-container" style="vertical-align:top">
            <input type="email" id="input_4" name="q4_email" class="form-textbox validate[Email]" size="30" value="" data-component="email" aria-labelledby="label_4 sublabel_input_4" />
            <label class="form-sub-label" for="input_4" id="sublabel_input_4" style="min-height:13px" aria-hidden="false"> example@example.com </label>
          </span>
          </div>
        </div>
        <div class="form-line" data-type="control_textbox" id="id_11">
          <label class="form-label form-label-top form-label-auto" id="label_11" for="input_11"> Phone Number </label>
          <div id="cid_11" class="form-input-wide">
            <input type="text" id="input_11" name="q11_phoneNumber11" data-type="input-textbox" class="form-textbox" size="15" value="" data-component="textbox" aria-labelledby="label_11" />
          </div>
        </div>
      </div>
      <div class="column">
        <div class="form-line" data-type="control_textbox" id="id_12">
          <label class="form-label form-label-top form-label-auto" id="label_12" for="input_12"> Country </label>
          <div id="cid_12" class="form-input-wide">
            <input type="text" id="input_12" name="q12_country" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_12" />
          </div>
        </div>
        <div class="form-line" data-type="control_textbox" id="id_8">
          <label class="form-label form-label-top form-label-auto" id="label_8" for="input_8"> Company Name </label>
          <div id="cid_8" class="form-input-wide">
            <input type="text" id="input_8" name="q8_companyName" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_8" />
          </div>
        </div>
        <div class="form-line" data-type="control_textbox" id="id_13">
          <label class="form-label form-label-top form-label-auto" id="label_13" for="input_13"> State / Province </label>
          <div id="cid_13" class="form-input-wide">
            <input type="text" id="input_13" name="q13_state" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" aria-labelledby="label_13" />
          </div>
        </div>
      </div>
      <div class="column">
        <div class="form-line" data-type="control_fileupload" id="id_14">
          <label class="form-label form-label-top form-label-auto" id="label_14" for="input_14">  </label>
          <div id="cid_14" class="form-input-wide">
            <div data-wrapper-react="true">
              <div data-wrapper-react="true">
                <div class="qq-uploader-buttonText-value">
                  Upload your Requests (Optional)
                </div>
                <input type="file" id="input_14" name="q14_input14[]" multiple="" class="form-upload-multiple" data-imagevalidate="yes" data-file-accept="pdf, doc, docx, xls, xlsx, csv, txt, rtf, html, zip, mp3, wma, mpg, flv, avi, jpg, jpeg, png, gif" data-file-maxsize="10854" data-file-minsize="0" data-file-limit="" data-component="fileupload" />
              </div>
              <span style="display:none" class="cancelText">
              Cancel
            </span>
              <span style="display:none" class="ofText">
              of
            </span>
            </div>
          </div>
        </div>
        <div class="form-line" data-type="control_textarea" id="id_10">
          <label class="form-label form-label-top form-label-auto" id="label_10" for="input_10"> Questions / Comments </label>
          <div id="cid_10" class="form-input-wide">
            <textarea id="input_10" class="form-textarea" name="q10_questions" cols="40" rows="6" data-component="textarea" aria-labelledby="label_10"></textarea>
          </div>
        </div>
      </div>
      <div class="buttons-set">
        <div class="form-line" data-type="control_button" id="id_2">
          <div id="cid_2" class="form-input-wide">
            <div style="margin-left:156px" data-align="auto" class="form-buttons-wrapper form-buttons-auto   jsTest-button-wrapperField">
              <button id="input_2"
                      type="submit"
                      class="form-submit-button submit-button jf-form-buttons jsTest-submitField et_pb_button"
                      data-component="button"
                      data-content="">
                Submit
              </button>
            </div>
          </div>
        </div>
        <div style="display:none">
          Should be Empty:
          <input type="text" name="website" value="" />
        </div>
      </div>
    </div>
  </div>
  <script>
      JotForm.showJotFormPowered = "0";
  </script>
  <script>
      JotForm.poweredByText = "Powered by JotForm";
  </script>
  <input type="hidden" class="simple_spc" id="simple_spc" name="simple_spc" value="202537074060043" />
  <script type="text/javascript">
      var all_spc = document.querySelectorAll("form[id='202537074060043'] .si" + "mple" + "_spc");
      for (var i = 0; i < all_spc.length; i++)
      {
          all_spc[i].value = "202537074060043-202537074060043";
      }
  </script>
</form>
<script type="text/javascript">JotForm.ownerView=true;</script>